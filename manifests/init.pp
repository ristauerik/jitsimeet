# @summary Main class, includes all other classes.
#
# @param jitsi_domain
#   The jitsi meet's fully qualified domain name.
#
# @param repo_key
#   Path to the _dearmored_ jitsi meet Debian repository GPG key.
#   Fingerprint should match 66A9 CD05 95D6 AFA 2472  90D3 BEF8 B479 E2DC 1389C
#
# @param jitsi_manage_source
#    If jitsi apt source should be managed by this module
#
# @param manage_certs
#   If the TLS certificates for FQDN and auth.FQDN should be managed using
#   Let's Encrypt. This requires a valid DNS entry for those domains.
#
# @param prosody_authentication_method
#   The authentication method used to connect to prosody.
#
# @param jitsi_vhost_additional_options
#   Additional options to pass to the prosody vhost.
#
# @param jicofo_additional_properties,
#   Additional properties to pass to jicofo.
#
# @param videobridge_additional_properties,
#   Additional properties to pass to videobridge.
#
# @param jitsi_vhost_ssl_key
#   Path to the jitsi virtualhost ssl key.
#
# @param jitsi_vhost_ssl_cert
#   Path to the jitsi virtualhost ssl certificate.
#
# @param auth_vhost_ssl_key
#   Path to the authentication virtualhost ssl key.
#
# @param auth_vhost_ssl_cert
#   Path to the authentication virtualhost ssl certificate.
#
# @param jvb_secret
#   The jitsi-videobridge component's secret.
#
# @param jvb_daemon_options
#   Options to pass to the jitsi-videobridge daemon.
#
# @param jvb_max_memory
#   The maximum memory in megabytes the jvb java process can consume.
#
# @param focus_secret
#   The focus component's secret.
#
# @param focus_user_password
#   Password for the focus user.
#
# @param meet_custom_options
#   Custom options to pass to the main jitsi configuration file.
#
# @param meet_interface_options
#   Custom options to pass to configure the jitsi meet web interface.
#
# @param www-root
#   Jiti meet's root directory for the webserver.
#
# @param nginx_manage_repo
#   If nginx should be installed from the upstream repository.
#
# @param webserver
#   The webserver you want to use to manage nginx. This parameter won't do
#   anything if you don't add the webserver in the list of managed services.
#
# @param @managed_services
#   The list of services this module should manage for you.
#
# @param jitsi_host
#   The IP or FQDN of the jitsi host. Use for internal connections by jitsi
#   services to the Prosody server.
#
class jitsimeet (
  Stdlib::Fqdn         $jitsi_domain,
  Boolean              $manage_certs,
  String               $repo_key,
  Boolean              $jitsi_manage_source,
  String[1]            $prosody_authentication_method,
  Hash                 $jitsi_vhost_additional_options,
  Hash                 $jicofo_additional_properties,
  Hash                 $videobridge_additional_properties,
  Optional[String]     $jitsi_vhost_ssl_key,
  Optional[String]     $jitsi_vhost_ssl_cert,
  Optional[String]     $auth_vhost_ssl_key,
  Optional[String]     $auth_vhost_ssl_cert,
  String               $jvb_secret,
  String               $jvb_daemon_options,
  Integer              $jvb_max_memory,
  String               $focus_secret,
  String               $focus_user_password,
  Hash                 $meet_custom_options,
  Hash                 $meet_interface_options,
  Stdlib::Absolutepath $www_root,
  Boolean              $nginx_manage_repo,
  Enum['nginx']        $webserver,
  Array[Enum['jicofo', 'jitsi-videobridge', 'webserver']] $managed_services,
  String               $jitsi_host,
) {

  include ::jitsimeet::config
  include ::jitsimeet::install

  if 'jitsi-videobridge' in $managed_services {
    if $jitsi_manage_source {
    Apt::Source['jitsimeet']
    -> Class['jitsimeet::videobridge::install']
    }
    contain 'jitsimeet::videobridge'
  }

  if 'jicofo' in $managed_services {
    if $jitsi_manage_source {
    Apt::Source['jitsimeet']
    -> Class['jitsimeet::jicofo::install']
    }
    contain 'jitsimeet::jicofo'
    contain 'jitsimeet::meet'
    include 'jitsimeet::prosody'
  }

  if 'webserver' in $managed_services {
    contain 'jitsimeet::webserver'
  }

}
