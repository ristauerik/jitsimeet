# @summary Manages apt sources.
#
class jitsimeet::install {

  if($jitsimeet::jitsi_manage_source){

    apt::source {
      'jitsimeet':
        comment  => 'Jitsi Meet',
        location => '[signed-by=/usr/share/keyrings/jitsimeet.gpg] https://download.jitsi.org',
        repos    => '',
        release  => 'stable/';
    }

    apt::pin {
      'jitsimeet':
        packages   => flatten([
          lookup('jitsimeet::meet::package_name', Variant[Array[String], String], 'first'),
          lookup('jitsimeet::jicofo::package_name', Variant[Array[String], String], 'first'),
          lookup('jitsimeet::videobridge::package_name', Variant[Array[String], String], 'first'),
          ]),
        priority   => 1000,
        originator => 'jitsi.org',
        label      => 'Jitsi Debian packages repository',
        codename   => 'stable';

      'jitsimeet-negative':
        packages   => '*',
        priority   => 200,
        originator => 'jitsi.org',
        label      => 'Jitsi Debian packages repository',
        codename   => 'stable';
    }

  }

  file { '/var/local/jitsimeet.preseed':
    content => epp('jitsimeet/jitsimeet.preseed.epp', {
      'focus_user_password' => $jitsimeet::focus_user_password,
      'focus_secret'        => $jitsimeet::focus_secret,
      'jitsi_domain'        => $jitsimeet::jitsi_domain,
      'jvb_secret'          => $jitsimeet::jvb_secret,
      });

    '/usr/share/keyrings/jitsimeet.gpg':
      ensure => present,
      source => $jitsimeet::repo_key,
      owner  => 'root',
      group  => 'root',
      mode   => '0644';
  }

}
